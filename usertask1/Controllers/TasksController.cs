﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using UserTask.Data;
using UserTask.DTOs;
using UserTask.Models;

namespace UserTask.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TasksController : ControllerBase
    {
        private readonly UserData userData;
        public TasksController(UserData userData)
        {
            this.userData = userData;
        }

        [HttpPost("AddUserWithTask")]

        public IActionResult Post([FromBody]UserDto userDto)
        {
            try
            {
                List<TaskUser> tasksUsers = new List<TaskUser>();
                
                User user = new User
                {
                    Name = userDto.Name,
                };
                
                foreach(var item in userDto.Tasks)
                {
                    TaskUser taskUser = new TaskUser
                    {
                        task_detail  = item.TaskDetail,
                    };
                    tasksUsers.Add(taskUser);
                }

                bool result = userData.Post(user, tasksUsers);
                if (result)
                    return StatusCode(201, user.pk_users_id);
               return StatusCode(500, "error");
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message); 

            }
        }

        [HttpGet("GetUserWithTasks")]

        public async Task<IActionResult> Get()
        {
            try
            {
                var user = await userData.GetAll();
                return Ok(user);
            }catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [HttpGet("GetUserWithTask")]
        public async Task<IActionResult> GetByName(string name)
        {
            try
            {
                var user = await userData.GetByName(name);
                if(user == null)
                    return NotFound();
                return Ok(user);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }
    }
}
