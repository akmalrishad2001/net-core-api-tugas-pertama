﻿using UserTask.Models;

namespace UserTask.DTOs
{
    public class UserDto
    {
        public string Name { get; set; } = string.Empty;

        public List<TaskDto> Tasks { get; set; } = new List<TaskDto>();
    }
}
