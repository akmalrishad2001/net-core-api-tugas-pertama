﻿using System.Data.SqlClient;
using Microsoft.AspNetCore.Server.Kestrel.Core.Features;
using Microsoft.Data.SqlClient;
using UserTask.DTOs;
using UserTask.Models;

namespace UserTask.Data
{
    public class UserData
    {

        private readonly IConfiguration _configuration;
        private readonly string ConnectionString;
        public UserData(IConfiguration configuration)
        {
            _configuration = configuration;

            ConnectionString = _configuration.GetConnectionString("DefaultConnection");

        }

        public bool Post(User user, List<TaskUser> tasksUser)
        {
            bool result = false;

            using (SqlConnection conn = new SqlConnection(ConnectionString))
            {
                conn.Open();

                SqlTransaction tran = conn.BeginTransaction();

                try
                {
                    SqlCommand cmdUser = new SqlCommand();
                    cmdUser.Connection = conn;
                    cmdUser.Transaction = tran;
                    cmdUser.Parameters.Clear();
                    cmdUser.CommandText = "INSERT INTO Users values (@name);";
                    cmdUser.Parameters.AddWithValue("@name", user.Name);
                    cmdUser.ExecuteNonQuery();

                    SqlCommand cmdTask = new SqlCommand();

                    foreach (var task in tasksUser)
                    {

                        cmdTask.Connection = conn;
                        cmdTask.Transaction = tran;
                        cmdTask.Parameters.Clear();
                        cmdTask.CommandText = "INSERT INTO Tasks values (@taskDetail,IDENT_CURRENT('Users'))";
                        cmdTask.Parameters.AddWithValue("@taskDetail", task.task_detail);
                        cmdTask.ExecuteNonQuery();
                    }




                    tran.Commit();

                    result = true;

                }
                catch (Exception ex)
                {
                    tran.Rollback();
                    throw;
                }
                finally
                {
                    conn.Close();
                }
            }

            return result;
        }

        public async Task<IEnumerable<UserWTask>> GetAll()
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                await connection.OpenAsync();

                string query = "select u.pk_users_id, u.name, t.pk_tasks_id, t.task_detail from users u left join tasks t on fk_users_id = pk_users_id;";

                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    List<UserWTask> users = new List<UserWTask>();
                    Dictionary<int, UserWTask> userDictionary = new Dictionary<int, UserWTask>();

                    using (SqlDataReader reader = await command.ExecuteReaderAsync())
                    {
                        while (reader.Read())
                        {
                            int userId = reader.GetInt32(0);

                            if (!userDictionary.TryGetValue(userId, out UserWTask user))
                            {
                                user = new UserWTask
                                {
                                    pk_users_id = userId,
                                    name = reader.GetString(1),
                                    Tasks = new List<TaskUser>()
                                };

                                userDictionary.Add(userId, user);
                                users.Add(user);
                            }

                            if (!reader.IsDBNull(2) && !reader.IsDBNull(3))
                            {
                                TaskUser task = new TaskUser
                                {
                                    pk_tasks_id = reader.GetInt32(2),
                                    task_detail = reader.GetString(3)
                                };

                                user.Tasks.Add(task);
                            }
                        }
                    }

                    return users;
                }
            }
        }

        public async Task<IEnumerable<UserWTask>>? GetByName(string name)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                await connection.OpenAsync();

                string query = "select u.pk_users_id, u.name, t.pk_tasks_id, t.task_detail from users u left join tasks t on fk_users_id = pk_users_id where u.name = @name;";

                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    command.Parameters.Clear();
                    command.Parameters.AddWithValue("@name", name);

                    List<UserWTask> users = new List<UserWTask>();
                    Dictionary<int, UserWTask> userDictionary = new Dictionary<int, UserWTask>();

                    using (SqlDataReader reader = await command.ExecuteReaderAsync())
                    {
                        while (reader.Read())
                        {
                            int userId = reader.GetInt32(0);

                            if (!userDictionary.TryGetValue(userId, out UserWTask user))
                            {
                                user = new UserWTask
                                {
                                    pk_users_id = userId,
                                    name = reader.GetString(1),
                                    Tasks = new List<TaskUser>()
                                };

                                userDictionary.Add(userId, user);
                                users.Add(user);
                            }

                            if (!reader.IsDBNull(2) && !reader.IsDBNull(3))
                            {
                                TaskUser task = new TaskUser
                                {
                                    pk_tasks_id = reader.GetInt32(2),
                                    task_detail = reader.GetString(3)
                                };

                                user.Tasks.Add(task);
                            }
                        }
                    }

                    return users;
                }
            }
        }
    }
}
